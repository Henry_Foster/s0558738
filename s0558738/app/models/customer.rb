class Customer < ApplicationRecord
  has_many :articles, dependent: :destroy
  validates :name, presence: true,
            length: { minimum: 3 }
end
