class ArticlesController < ApplicationController
  def create
    @customer = Customer.find(params[:customer_id])
    @article = @customer.articles.create(article_params)
    redirect_to customer_path(@customer)
  end

  def destroy
    @customer = Customer.find(params[:customer_id])
    @article = @customer.articles.find(params[:id])
    @article.destroy
    redirect_to customer_path(@customer)
  end

  private
  def article_params
    params.require(:article).permit(:name, :body)
  end
end
